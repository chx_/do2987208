<?php

namespace Drupal\do2987208;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

class Do2987208ServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container) {
    $container->getDefinition('plugin.manager.element_info')
      ->setClass(AlterableElementInfoManager::class);
  }


}
