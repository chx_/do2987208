<?php

namespace Drupal\do2987208;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\ElementInfoManager;
use Drupal\Core\Theme\ThemeManagerInterface;

class AlterableElementInfoManager extends ElementInfoManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, CacheTagsInvalidatorInterface $cache_tag_invalidator, ModuleHandlerInterface $module_handler, ThemeManagerInterface $theme_manager) {
    parent::__construct($namespaces, $cache_backend, $cache_tag_invalidator, $module_handler, $theme_manager);
    $this->alterInfo('do2987208_element_info');
  }

}
